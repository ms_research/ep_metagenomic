Author: Melanie Schirmer, The Broad Institute of MIT and Harvard, mail@melanieschirmer.com


###############
### USAGE
###############

ErrorProfiles_wQ_MG_v1.0.sh <Output_Prefix> <NO_Cores> <file.Calmd> <file.sam> <max_read_length>

The program calculates position and nucleotide-specific error profiles for metagenomic datasets
and requires the following input parameters:
    - A prefix for the output files (no whitespaces in prefix). A directory with the same name
      will be created in which the output files are saved. 
    - The number of cores that should be used for the computations.
    - A file with the aligned (!) reads and extended MD tag. Only aligned reads should be included.
    - A file in sam format with the aligned (!) reads. Only aligned reads should be included.
    - The maximum read length (e.g. the original length of the sequenced reads).

For additional information and an example see the tutorial in the Test_Dataset directory.  

Additional options:
    -h  shows this help text

Never run multiple copies of the script in the same directory.


###############
#### DEPENDENCIES
################

Perl             The scripts for the computations are implemented in Perl. (Tested under version v5.10.1.)
                 Ensure that your perl distribution supports:
                   - feature (Perl pragma to enable new features)
                   - Scalar::Util (A selection of general-utility scalar subroutines)

Bash             The main bash script combines the perl scripts and allows to run computation in parallel
GNU parallel     (using GNU parallel).

R                The graphs are generated in R. (Tested under version version 3.0.0.)


###############
### CITATION 
###############

Melanie Schirmer, Rosalinda D’Amore, Umer Z. Ijaz, Neil Hall and Christopher Quince. Illumina Error Profiles: Resolving Fine-Scale Variation in Metagenomic Sequencing Data. [under review]




###############
### SUPPORT
###############

If you would like to report a bug, please contact: mail@melanieschirmer.com



