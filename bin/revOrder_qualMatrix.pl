#!/usr/bin/perl

#Usage: ./revOrder_qualMatrix.pl Read_Length qualMatrix_A_rev qualMatrix_T_rev qualMatrix_G_rev qualMatrix_C_rev qualMatrix_A_fwd qualMatrix_T_fwd qualMatrix_G_fwd qualMatrix_C_fwd new_qualMatrix_A_all new_qualMatrix_T_all new_qualMatrix_G_all new_qualMatrix_C_all

use strict; use warnings; use diagnostics;

my $length1 = $ARGV[0];
my $errorA = $ARGV[1];
my $errorT = $ARGV[2];
my $errorG = $ARGV[3];
my $errorC = $ARGV[4];
my $errorIns = $ARGV[5];
my $errorDel = $ARGV[6];
my $errorUnknown = $ARGV[7];
my $errorA_fwd = $ARGV[8];
my $errorT_fwd = $ARGV[9];
my $errorG_fwd = $ARGV[10];
my $errorC_fwd = $ARGV[11];
my $errorIns_fwd = $ARGV[12];
my $errorDel_fwd = $ARGV[13];
my $errorUnknown_fwd = $ARGV[14];
my $new_errorA = $ARGV[15];
my $new_errorT = $ARGV[16];
my $new_errorG = $ARGV[17];
my $new_errorC = $ARGV[18];
my $new_errorIns = $ARGV[19];
my $new_errorDel = $ARGV[20];
my $new_errorUnknown = $ARGV[21];


open(FILE_A, $errorA) or die "Can't open $errorA\n";
open(FILE_T, $errorT) or die "Can't open T\n";
open(FILE_G, $errorG) or die "Can't open G\n";
open(FILE_C, $errorC) or die "Can't open C\n";
open(FILE_Ins, $errorIns) or die "Can't open Ins\n";
open(FILE_Del, $errorDel) or die "Can't open Del\n";
open(FILE_Unknown, $errorUnknown) or die "Can't open Unknown\n";

open(FILE_A_fwd, $errorA_fwd) or die "Can't open A fwd\n";
open(FILE_T_fwd, $errorT_fwd) or die "Can't open T fwd\n";
open(FILE_G_fwd, $errorG_fwd) or die "Can't open G fwd\n";
open(FILE_C_fwd, $errorC_fwd) or die "Can't open C fwd\n";
open(FILE_Ins_fwd, $errorIns_fwd) or die "Can't open Ins fwd\n";
open(FILE_Del_fwd, $errorDel_fwd) or die "Can't open Del fwd\n";
open(FILE_Unknown_fwd, $errorUnknown_fwd) or die "Can't open Unknown fwd\n";

open(my $FILE_A_all, '>', $new_errorA) or die "Can't open errorMatrix_A_rev.txt\n";
open(my $FILE_T_all, '>', $new_errorT)	or die "Can't open errorMatrix_T_rev.txt\n";
open(my $FILE_G_all, '>', $new_errorG)	or die "Can't open errorMatrix_G_rev.txt\n";
open(my $FILE_C_all, '>', $new_errorC)	or die "Can't open errorMatrix_C_rev.txt\n";
open(my $FILE_Ins_all, '>', $new_errorIns)  or die "Can't open errorMatrix_Ins_rev.txt\n";
open(my $FILE_Del_all, '>', $new_errorDel)  or die "Can't open errorMatrix_Del_rev.txt\n";
open(my $FILE_Unknown_all, '>', $new_errorUnknown)  or die "Can't open errorMatrix_Unknown_rev.txt\n";


my @subA = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subT = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subG = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subC = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subIns = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subDel = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subUnknown = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

my @A = (\@subA);
my @T = (\@subT);
my @G = (\@subG);
my @C = (\@subC);
my @Ins = (\@subIns);
my @Del = (\@subDel);
my @Unknown = (\@subUnknown);

for (my $j=1;$j<$length1;$j++){
	#print "$j\n";
	my @subA = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	push @A, \@subA;
	my @subT = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	push @T, \@subT;
	my @subG = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	push @G, \@subG;
	my @subC = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	push @C, \@subC;
	my @subIns = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @Ins, \@subIns;
	my @subDel = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @Del, \@subDel;
	my @subUnknown = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @Unknown, \@subUnknown;
}

my @subA_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subT_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subG_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subC_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subIns_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subDel_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @subUnknown_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

my @A_fwd = (\@subA_fwd);
my @T_fwd = (\@subT_fwd);
my @G_fwd = (\@subG_fwd);
my @C_fwd = (\@subC_fwd);
my @Ins_fwd = (\@subIns_fwd);
my @Del_fwd = (\@subDel_fwd);
my @Unknown_fwd = (\@subUnknown_fwd);

for (my $j=1;$j<$length1;$j++){
        #print "$j\n";
        my @subA_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @A_fwd, \@subA_fwd;
        my @subT_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @T_fwd, \@subT_fwd;
        my @subG_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @G_fwd, \@subG_fwd;
        my @subC_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @C_fwd, \@subC_fwd;
	my @subIns_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @Ins_fwd, \@subIns_fwd;
	my @subDel_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @Del_fwd, \@subDel_fwd;
	my @subUnknown_fwd = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @Unknown_fwd, \@subUnknown_fwd;
}



my $line=0;
my $i=0;
my $j=0;
my @temp=();
my $length=0;

while($line = <FILE_A>){
	#print "$line";
	chomp($line);
	@temp=split("\t",$line);
	for($j=0;$j<50;$j++){
		$A[$length][$j] = $temp[$j];
	}
	#print "$A[$length][1]\t$A[$length][0]\t$A[$length][3]\t$A[$length][2]\n";
	$length = $length +1;
}


$length=0;
while($line = <FILE_A_fwd>){
        #print "$line";
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){        
		$A_fwd[$length][$j] = $temp[$j];
	}
        #print "$A_fwd[$length][1]\t$A_fwd[$length][0]\t$A_fwd[$length][3]\t$A_fwd[$length][2]\n";
        $length = $length +1;
}



$length=0;
while($line = <FILE_T>){
        chomp($line);
	@temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $T[$length][$j] = $temp[$j];
	}
	#print "$T[$length][1]\t$T[$length][0]\t$T[$length][3]\t$T[$length][2]\n";
	$length = $length +1;
}


$length=0;
while($line = <FILE_T_fwd>){
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $T_fwd[$length][$j] = $temp[$j];
	}
        #print "$T_fwd[$length][0]\t$T_fwd[$length][1]\t$T_fwd[$length][2]\t$T_fwd[$length][3]\n";
        $length = $length +1;
}





$length=0;
while($line = <FILE_G>){
        chomp($line);
	@temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $G[$length][$j] = $temp[$j];
	}
	$length = $length +1;
}


$length=0;
while($line = <FILE_G_fwd>){
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $G_fwd[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}







$length=0;
while($line = <FILE_C>){
        chomp($line);
	@temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $C[$length][$j] = $temp[$j];
	}
	$length = $length +1;
}

$length=0;
while($line = <FILE_C_fwd>){
        chomp($line);
        @temp=split("\t",$line);
        for($j=0;$j<50;$j++){
		$C_fwd[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}



$length=0;
while($line = <FILE_Ins>){
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $Ins[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}

$length=0;
while($line = <FILE_Ins_fwd>){
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $Ins_fwd[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}


$length=0;
while($line = <FILE_Del>){
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $Del[$length][$j] = $temp[$j];
	}	
        $length = $length +1;
}

$length=0;
while($line = <FILE_Del_fwd>){
        chomp($line);
        @temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $Del_fwd[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}



$length=0;
while($line = <FILE_Unknown>){
        chomp($line);
	@temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $Unknown[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}

$length=0;
while($line = <FILE_Unknown_fwd>){
        chomp($line);
	@temp=split("\t",$line);
	for($j=0;$j<50;$j++){
	        $Unknown_fwd[$length][$j] = $temp[$j];
	}
        $length = $length +1;
}





for($i=0; $i<$length1;$i++){
	#print "$T[$i][1]\t$T[$i][0]\t$T[$i][3]\t$T[$i][2]\n";
	#print "$T_fwd[$i][1]\t$T_fwd[$i][0]\t$T_fwd[$i][3]\t$T_fwd[$i][2]\n";
	for($j=0;$j<50;$j++){
		print $FILE_A_all "${\($T[$length1-1-$i][$j]+$A_fwd[$i][$j])}\t"; 
		print $FILE_T_all "${\($A[$length1-1-$i][$j]+$T_fwd[$i][$j])}\t";
		print $FILE_G_all "${\($C[$length1-1-$i][$j]+$G_fwd[$i][$j])}\t";
		print $FILE_C_all "${\($G[$length1-1-$i][$j]+$C_fwd[$i][$j])}\t";
	
		print $FILE_Ins_all "${\($Ins[$length1-1-$i][$j]+$Ins_fwd[$i][$j])}\t";
		print $FILE_Del_all "${\($Del[$length1-1-$i][$j]+$Del_fwd[$i][$j])}\t";
		print $FILE_Unknown_all "${\($Unknown[$length1-1-$i][$j]+$Unknown_fwd[$i][$j])}\t";
	}
	print $FILE_A_all "\n";
	print $FILE_T_all "\n";
	print $FILE_G_all "\n";
	print $FILE_C_all "\n";
	print $FILE_Ins_all "\n";
	print $FILE_Del_all "\n";
	print $FILE_Unknown_all "\n";
}



close($FILE_A_all);
close($FILE_T_all);
close($FILE_G_all);
close($FILE_C_all);
close($FILE_Ins_all);
close($FILE_Del_all);
close($FILE_Unknown_all);

close(FILE_A_fwd);
close(FILE_T_fwd);
close(FILE_G_fwd);
close(FILE_C_fwd);
close(FILE_Ins_fwd);
close(FILE_Del_fwd);
close(FILE_Unknown_fwd);

close(FILE_A);
close(FILE_T);
close(FILE_G);
close(FILE_C);
close(FILE_Ins);
close(FILE_Del);
close(FILE_Unknown);


